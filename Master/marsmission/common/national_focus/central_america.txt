focus_tree = {
    id = centralamerica
    
    
    country = {
        factor = 0
        
        modifier = {
            add = 20
            tag = CTA
        }
    }
    focus = {
        id = CTA_prepare_elections
        icon = GFX_goal_tripartite_pact
        x = 0
        y = 0
        cost = 4
        completion_reward = {
            add_political_power = 100
        }
    }
    focus = {
        id = CTA_election
        icon = GFX_focus_usa_voter_registration_act
        x = 0
        y = 1
        cost = 1
        prerequisite = {
            focus = CTA_prepare_elections
        }
        completion_reward = {
            country_event = {
                id = centroelection.1
            }
        }
    }
    focus = {
        id = CTA_left_victory
        icon = GFX_focus_usa_voter_registration_act
        x = -10
        y = 2
        cost = 0
        prerequisite = {
            focus = CTA_election
        }
        completion_reward = {
            add_political_power = 100
        }
    }
    focus = {
        id = CTA_center_victory
        icon = GFX_focus_usa_voter_registration_act
        x = 0
        y = 2
        cost = 0
        prerequisite = {
            focus = CTA_election
        }
        completion_reward = {
            add_political_power = 100
        }
    }
    focus = {
        id = CTA_nationalist_victory
        icon = GFX_focus_usa_voter_registration_act
        x = 10
        y = 2
        cost = 0
        prerequisite = {
            focus = CTA_election
        }
        completion_reward = {
            add_political_power = 100
        }
    }
}