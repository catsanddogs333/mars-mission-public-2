sub_units = {

	garrison = {
		sprite = infantry
		map_icon_category = infantry

		priority = 599
		ai_priority = 200
		active = yes

		type = {
			infantry
		}

		group = infantry

		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}

		combat_width = 2

		#Size Definitions
		max_strength = 30
		max_organisation = 60
		default_morale = 0.30
		manpower = 1000

		#Misc Abilities
		defense = 2.0
		soft_attack = -0.5
		hard_attack = -0.5
		training_time = 70
		suppression = 3
		weight = 0.5
		breakthrough = -0.5	# Garrisons don't know how to attack, get slaughtered

		supply_consumption = 0.05

		maximum_speed = -0.25

		need = {
			infantry_equipment = 100
		}

		forest = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		mountain = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		jungle = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		marsh = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		hills = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		mountain = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		plains = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		urban = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		desert = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		river = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
		amphibious = {
			attack = -0.3
			movement = -0.5
			defence = +0.6
		}
	}
}