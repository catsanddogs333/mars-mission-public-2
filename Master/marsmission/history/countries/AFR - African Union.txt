capital = 1

set_politics = {
	ruling_party = authoritarian_democrat
	last_election = "2200.11.8"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	anarchist = 3
	collectivist = 14
	social_democrat = 12
	liberal = 0
	centrism = 21
	conservative = 0
	authoritarian_democrat = 47
	paternal_autocrat = 1
	national_populist = 2
	monarchist = 0
}