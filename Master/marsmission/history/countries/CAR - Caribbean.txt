capital = 64

set_politics = {
	ruling_party = social_democrat
	last_election = "2200.11.8"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	anarchist = 0
	collectivist = 24
	social_democrat = 43
	liberal = 16
	centrism = 3
	conservative = 14
	authoritarian_democrat = 0
	paternal_autocrat = 0
	national_populist = 0
	monarchist = 0
}