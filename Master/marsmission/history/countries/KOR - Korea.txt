﻿capital = 1

set_politics = {
	ruling_party = liberal
	last_election = "2200.11.8"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	anarchist = 0
	collectivist = 12
	social_democrat = 17
	liberal = 32
	centrism = 0
	conservative = 31
	authoritarian_democrat = 7
	paternal_autocrat = 0
	national_populist = 1
	monarchist = 0
}