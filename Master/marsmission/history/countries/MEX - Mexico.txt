﻿capital = 1

set_politics = {
	ruling_party = conservative
	last_election = "2200.11.8"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	anarchist = 0
	collectivist = 4
	social_democrat = 9
	liberal = 15
	centrism = 0
	conservative = 49
	authoritarian_democrat = 6
	paternal_autocrat = 0
	national_populist = 17
	monarchist = 0
}