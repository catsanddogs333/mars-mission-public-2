﻿capital = 1

set_politics = {
	ruling_party = centrism
	last_election = "2200.11.8"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
	anarchist = 0
	collectivist = 0
	social_democrat = 19
	liberal = 10
	centrism = 34
	conservative = 32
	authoritarian_democrat = 5
	paternal_autocrat = 0
	national_populist = 0
	monarchist = 0
}