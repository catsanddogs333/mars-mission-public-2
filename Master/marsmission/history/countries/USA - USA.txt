﻿capital = 277

set_politics = {
	ruling_party = liberal
	last_election = "2200.11.8"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	anarchist = 0
	collectivist = 2
	social_democrat = 14
	liberal = 40
	centrism = 0
	conservative = 38
	authoritarian_democrat = 4
	paternal_autocrat = 0
	national_populist = 2
	monarchist = 0
}