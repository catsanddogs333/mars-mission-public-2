state={
	id=377
	name="Cerberus Plains"
	state_category = town
	history={
		owner = AST
		buildings = {
			infrastructure = 4

		}
		victory_points = {
			9964 5
		}
		add_core_of = AST

	}
	provinces={
		137 598 612 1239 1786 3452 3555 3880 4592 5699 5805 5871 6411 6599 6701 6849 7325 9153 9348 9915 9947 9964 9975 10042 
	}
	manpower=876710
	buildings_max_level_factor=1.000
}
